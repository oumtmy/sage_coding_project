### Definition of the methods
def mc_eliece_key_gen(C):
    G = C.generator_matrix()
    S = random_matrix(C.base_field(), C.dimension(), C.dimension())
    while S.is_singular():
        S = random_matrix(C.base_field(), C.dimension(), C.dimension())
    sigma = range(0, C.length())
    shuffle(sigma)
    P = matrix(C.base_field(), C.length(), C.length())
    for i in sigma:
        P[i,sigma[i]] = 1
    G_pub = S*G*P
    t = C.decoder().decoding_radius()
    pubkey = (G_pub, t)
    privkey = (S.inverse(), G, P.inverse(), C)
    return pubkey, privkey

def mc_eliece_encrypt(m, pubkey):
    G_pub, t = pubkey
    c = vector(m) * G_pub
    ambient = VectorSpace(G_pub.base_ring(), G_pub.ncols())
    channel = ChannelStaticErrorRate(ambient, t)
    y = channel.transmit(c)
    return y

def mc_eliece_decrypt(y, privkey):
    S, G, P, C = privkey
    y = y * P
    m = C.decode_to_message(y)
    m = m * S
    return m

### 

F = GF(59)
n, k =  40, 12
C = GeneralizedReedSolomonCode(F.list()[:n], k, F.list()[1:n+1])
pub, priv = mc_eliece_key_gen(C)

plaintext = vector(GF(59), (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37))

ciphertext = mc_eliece_encrypt(plaintext, pub)

###

deciphered = mc_eliece_decrypt(ciphertext, priv)

print plaintext
print deciphered

print plaintext == deciphered

###



