### Tests for the structure of Encoders and Decoders, in particular registration system
from sage.coding.encoder import Encoder
from sage.coding.decoder import Decoder
class CodeTest(LinearCode):

    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, length, dimension):
        self._length = length
        self._dimension = dimension
        self._cached_encoders = {}
        self._cached_decoders = {}

    def encoder_default(self):
        return "Enc1"

    def decoder_default(self):
        return "Dec1"

    def __repr__(self):
        return "A test code"

    def __eq__(self):
        return False


class TestEncoder1(Encoder):
    def __init__(self, C):
        #print "Construction TestEncoder1"
        pass

class TestEncoder2(Encoder):
    def __init__(self, C, myarg):
        #print "Constructing TestEncoder2: ", myarg
        self.myarg = myarg

class TestDecoder1(Decoder):
    def __init__(self, C):
        #print "Construction TestDecoder1"
        pass

class TestDecoder2(Decoder):
    def __init__(self, C, myarg):
        #print "Constructing TestDecoder2: ", myarg
        self.myarg = myarg

#TODO: Call registration function? (see Issue #50)
CodeTest._registered_encoders["Enc1"] = TestEncoder1
CodeTest._registered_encoders["Enc2"] = TestEncoder2
CodeTest._registered_decoders["Dec1"] = TestDecoder1
CodeTest._registered_decoders["Dec2"] = TestDecoder2

### Multiple construction of Encoders
C = CodeTest(2,1)
assert len(C.encoders_available()) == 2

Edef = C.encoder()
E = C.encoder("Enc1")
E = C.encoder("Enc1")
assert Edef == E , "Encoders should be the same" #note, memory equality

#TODO: myarg shouldn't be passed explicitly (maybe, see issue #48)
E = C.encoder("Enc2", myarg=3)
E = C.encoder("Enc2", myarg=3)
#TODO: Make some assertion

E4 = C.encoder("Enc2", myarg=4)
E5 = C.encoder("Enc2", myarg=5)
assert E4 != E5 , "Encoders should be different" #memory equality since __eq__ not implemented

### Multiple construction of Decoders
print "Verify that only one TestDecoder1 was constructed"
D = C.decoder_default()
D = C.decoder("Dec1")
D = C.decoder("Dec1")

print "\nVerify that only one TestDecoder2 was constructed"
#TODO: myarg shouldn't be passed explicitly (maybe, see issue #48)
D = C.decoder("Dec2", myarg=3)
D = C.decoder("Dec2", myarg=3)

print "\nVerify that MORE than one TestDecoder2 was constructed"
D4 = C.decoder("Dec2", myarg=4)
D5 = C.decoder("Dec2", myarg=5)
assert D4 != D5 , "Decoders should be different" #memory equality since __eq__ not implemented


### Multiple instances of code have different caches
C1 = CodeTest(2,1)
C2 = CodeTest(3,2)
print "Verify that two TestEncoder1 were constructed"
E1 = C1.encoder("Enc1")
E2 = C2.encoder("Enc1")
assert E1 != E2, "encoders for different codes should be different" #note, memory equality

print "Verify that two TestDecoder1 were constructed"
D1 = C1.decoder("Dec1")
D2 = C2.decoder("Dec1")
assert D1 != D2, "decoders for different codes should be different" #note, memory equality

