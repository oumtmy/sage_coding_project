### Test GRS code implementation
F = GF(59)
n, k = 40, 12
C = GeneralizedReedSolomonCode(F.list()[:n], k)
C
repr(C)
latex(C)

###Test GRS equality check
F1 = GF(59)
F2 = GF(61)
n1, k1 = 40, 12
n2, k2 = 42, 14
C1 = GeneralizedReedSolomonCode(F1.list()[:n1], k1, [F1.one()]*n1)
C2 = GeneralizedReedSolomonCode(F1.list()[:n1], k1, [F1.one()]*n1)
assert C1 == C2, "They should be equal!"
C2 = GeneralizedReedSolomonCode(F2.list()[:n1], k1, [F2.one()]*n1)
assert C1 != C2, "The fields are different!"
C2 = GeneralizedReedSolomonCode(F1.list()[:n2], k1, [F1.one()]*n2)
assert C1 != C2, "The lengths are different!"
C2 = GeneralizedReedSolomonCode(F1.list()[:n1], k2, [F1.one()]*n1)
assert C1 != C2, "The dimensions are different!"
C2 = GeneralizedReedSolomonCode([F1(n1)]+F1.list()[:n1-1], k1, [F1.one()]*n1)
assert C1 != C2, "The evaluation points are different!"
C2 = GeneralizedReedSolomonCode(F1.list()[:n1], k1, [F1.one()]*(n1-1)+[F1(2)])
assert C1 != C2, "The column multipliers are different!"

### basic getter-methods
assert n == C.length()
assert k == C.dimension()
print C.generator_matrix()
print C.parity_check_matrix()
assert n-k+1 == C.minimum_distance()
assert F.list()[:n] == C.evaluation_points()


### Playing with a codeword
w = vector( F.random_element() for i in range(k))
assert w in C.encoder().message_space()
print "default encoder:", C.encoder()
c = C.encode(w)
assert c in C
assert c in C.ambient_space()
assert C.unencode(c) == w
E = C.encoder()
assert c == E.encode(w)

c[1] += 1
assert not c in C
assert c in C.ambient_space()

c = C.random_element()
assert c in C
assert c in C.ambient_space()
w = C.unencode(c)
assert C.encode(w) == c

wz = vector([F.zero()]*k)
assert wz in C.encoder().message_space()
cz = C.encode(wz)
assert cz.is_zero()

wx = vector([F.zero(), F.one()] + [F.zero()]*(k-2))
assert wx in C.encoder().message_space()
cx = C.encode(wx)
assert all(cx[i] == C.evaluation_points()[i] for i in range(C.length()))

### Testing generic unencode
N = 100
for i in range(N):
    m = random_vector(C.base_field(), C.dimension())
    E = EncoderLinearCodeGeneratorMatrix(C) 
    c = E.encode(m)
    assert E.unencode(c) == m, "Initial word and unencoded codeword should be the same"
    
### Testing check on unencode
word = random_vector(C.base_field(), C.length())
while word in C:
    word = random_vector(C.base_field(), C.length())
try:
    C.unencode(word)
except sage.coding.encoder.EncodingFailure,e:
    print "Should be an EncodingFailure %s" % e
try:
    C.unencode(word, nocheck=True)
except sage.coding.encoder.EncodingFailure,e:
    "Should not raise an exception"

### Testing EncoderGRSEvaluationPolynomial
P = C.base_ring()['x']
p = P.random_element()
E = C.encoder("EvaluationPolynomial")
assert P == E.message_space(), "The two fields should be the same"
p_enc = E.encode(p)
assert p == E.unencode_nocheck(p_enc), "The two messages should be the same"

### Decoding of hard-decision errors
N = 100
dec_names = ["BerlekampWelch", "Gao"]
print "default decoder:", C.decoder()
assert all(dec in C.decoders_available() for dec in dec_names)
decoders = [ C.decoder(name) for name in dec_names ]
#TODO: Use introspection on decoders to get the full list of hard-decision decoders 
t = decoders[0].decoding_radius()
assert t == (C.minimum_distance()-1)//2
assert t == decoders[1].decoding_radius()
Ch = channels.StaticErrorRateChannel(C.ambient_space(), t)
for D in decoders:
    print "Testing decoder ", D
    for i in range(N):
        c = C.random_element()
        r = Ch.transmit(c)
        try:
            assert c == D.decode_to_code(r) , "Decoding did not return the correct codeword"
            #TODO: m = D.connected_encoder.unencode(c)
            #TODO: assert m == D.decode_to_message(r)
        except ValueError,e:
            assert False, "The codeword should have been decoded! Error:\n\t%s" % e 

### Decoding with syndrome and nearest neighbour on GRS
F = GF(7)
n, k = 7, 3
C = GeneralizedReedSolomonCode(F.list()[:n], k)
N = 100
dec_names = ["Syndrome", "NearestNeighbor"]
print "default decoder:", C.decoder()
assert all(dec in C.decoders_available() for dec in dec_names)
decoders = [ C.decoder(name) for name in dec_names ]
t = C.minimum_distance() - 1
Ch = channels.StaticErrorRateChannel(C.ambient_space(), (1,t))
for D in decoders:
    print "Testing decoder ", D
    for i in range(N):
        c = C.random_element()
        r = Ch.transmit(c)
        try:
            c_out = D.decode_to_code(r)
            assert c_out in C , "Returned word not a codeword"
            assert (c-r).hamming_weight() >= (c_out-r).hamming_weight() , "Returned word not closest"
        except ValueError,e:
            assert False, "The codeword should have been decoded! Error:\n\t%s" % e 


### ErrorErasure test
Ch = ChannelErrorErasure(C.ambient_space(), 2, 2)
c = C.random_element()
r = Ch.transmit(c)
try:
    c_out = C.decode_to_code(r, "ErrorErasure")
    assert c == c_out
except ValueError, e:
    assert False, "Codeword should have been decoded"

### Decoding of error-erasure-decision errors
#Choose randomly maximal errors and erasures
t = randint(0,(C.minimum_distance()-1)//2)
e = randint(0,C.minimum_distance() - 1 - 2*t)
Ch = ChannelErrorErasure(C.ambient_space(), t, e)
D = C.decoder("ErrorErasure")
print "Decoder: ", D
for i in range(N):
    c = C.random_element()
    r = Ch.transmit(c)
    assert r in D.input_space()
    sp = CartesianProduct(C.ambient_space(), VectorSpace(GF(2), C.ambient_space().dimension()))
    assert D.input_space() == sp, "Decoder input space is not the expected one"
    c_out = D.decode_to_code(r)
    assert c == c_out




# UNIT TESTS TRYING TO BLOW THINGS UP

### Does GRS refuse evaluation points which are not from a finite field?
evals = [i for i in range(n)]
try:
    C = GeneralizedReedSolomonCode(evals, k, [F.one()]*n)
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error because evals are not in a finite field:\n\t", e

### Does GRS refuse column multipliers which are not from a finite field?
colmults = [i for i in range(n)]
try:
    C = GeneralizedReedSolomonCode(F.list()[:n], k, colmults)
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error because colmults are not in a finite field:\n\t", e

### Does GRS refuse to construct if evals and col mults are not in the same field?
Fe = GF(59)
Fc = GF(61)
try:
    C = GeneralizedReedSolomonCode(Fe.list()[:n], k, Fc.list()[:n])
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error because colmults and evals are not in the same finite field:\n\t", e


### Does GRS remember to copy list of eval pts and col mults?
evals = F.list()[:n]
colmults = [F.one()]*n
C = GeneralizedReedSolomonCode(evals, k, colmults)
while evals:
    del evals[0]
    del colmults[0]
assert len(C.evaluation_points()) == n
assert len(C.column_multipliers()) == n

### Does GRS remember to check uniqueness of eval points?
evals = F.list()[:n-1] + [F.list()[1]]
try:
    C = GeneralizedReedSolomonCode(evals, k, [F.one()]*n)
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for equal eval points:\n\t", e

### Does GRS remember to check uniqueness of eval points after coercion?
F = GF(17)
n, k = 16, 10
try:
    C = GeneralizedReedSolomonCode([1, 18] + range(2,n-2), k)
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for equal eval points:\n\t", e

### Does GRS remember to check length of col-mults?
try:
    C = GeneralizedReedSolomonCode(F.list()[:n], k, [F.one()]*(n-1))
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for wrong len of column multipliers:\n\t", e
try:
    C = GeneralizedReedSolomonCode(F.list()[:n], k, [F.one()]*(n+1))
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for wrong len of column multipliers:\n\t", e


### Does GRS remember to check non-zero col-mults
try:
    C = GeneralizedReedSolomonCode(F.list()[:n], k, [F.zero()]+[F.one()]*(n-1))
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for non-zero column multipliers:\n\t", e

# This tries to pass an uncoerced 0 as well
try:
    C = GeneralizedReedSolomonCode(F.list()[:n], k, [0]+[F.one()]*(n-1))
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for non-zero column multipliers:\n\t", e


### Does GRS remember to check field of col-mults
F3 = GF(3)
try:
    C = GeneralizedReedSolomonCode(F.list()[:n], k, [F3(2)]+[F.one()]*(n-1))
    assert False , "Shouldn't be accepted"
except ValueError, e:
    print "This should be an error for field of column multipliers:\n\t", e


### Equality of decoders and encoders
F = GF(59)
n, k = 40, 12
C = GeneralizedReedSolomonCode(F.list()[:n], k, [F.one()]*n)
#NOTE: The following will fail once we make an en/decoder which takes an argument
encoders = [ C.encoder(n) for n in C.encoders_available() ]
assert len(encoders) == len(set(encoders))

decoders = [ C.decoder(n) for n in C.decoders_available() ]
assert len(decoders) == len(set(decoders))


### Code for building PC matrix in HammingCode
#def combine(e, ls):
#    return [e + le for le in ls]
#
#def select(F, t):
#    return cartesian_product(list([F.list() for i in range(t)]))
#    
#def test(F, n):
#    return flatten([combine([0] * l + [1], select(F, n-l-1)) for l in range(n)])
#
#test(GF(3), 3)

###
