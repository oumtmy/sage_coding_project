r"""
Concatenated codes

This file contains the following elements:

    - *ConcatenatedCode*, the class for the concatenated codes
    - *EncoderConcatenatedCodeOuterField*, an encoder which uses the message
      space of the outer code
    - *DecoderConcatenatedCodeInnerAndOuterDecoding*, a decoder which uses
      decoders from the inner code and the outer code
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from linear_code import AbstractLinearCode
from encoder import Encoder
from decoder import Decoder, DecodingFailure
from sage.modules.free_module_element import vector
from sage.modules.free_module import VectorSpace
from sage.rings.finite_rings.constructor import GF

class ConcatenatedCode(AbstractLinearCode):
    r"""
    Construct a Concatenated code.

    INPUT:

    - ``inner_code`` -- a linear code

    - ``outer_code`` -- a linear code
    """
    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, inner_code, outer_code):
        r"""
        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C
            Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code

        TESTS:

        If the base field of ``inner_code`` is not a prime field, an error is
        raised
        :

            sage: Cin = QAryHammingCode(GF(4, 'x'), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            Traceback (most recent call last):
            ...
            TypeError: The base field of the inner code must be a prime field

        The order of the base field of ``outer_code`` must be `q ** k` where
        `q` is the order of the base field of ``inner_code`` and `k``the
        dimension of ``inner_code``
        ::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(3^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            Traceback (most recent call last):
            ...
            ValueError: Order of outer code's base field must be q^k where q is order of inner code's base field and k is inner code's dimension
        """
        self._inner_code  = inner_code
        self._outer_code = outer_code
        length = inner_code.length() * outer_code.length()
        field = inner_code.base_field()
        k = inner_code.dimension()
        super(ConcatenatedCode, self).__init__(field, length)
        self._encoder_default_name = "OuterField"
        self._decoder_default_name = "InnerOuterDecode"

        if field.is_prime_field() == False:
            raise TypeError("The base field of the inner code must be a prime field")
        if outer_code.base_field().order() != field.order() ** k:
            raise ValueError("Order of outer code's base field must be q^k where q is order of inner code's base field and k is inner code's dimension")

    def __eq__(self, other):
        r"""
        Test equality of Concatenated code objects.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C1 = ConcatenatedCode(Cin, Cout)
            sage: C2 = ConcatenatedCode(Cin, Cout)
            sage: C1 == C2
            True
        """
        return isinstance(other, ConcatenatedCode) \
                and self.inner_code()  == other.inner_code() \
                and self.outer_code() == other.outer_code()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C
            Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code
        """
        return "Concatenated code with %s as outer code and %s as inner code"\
                % (repr(self.outer_code()), repr(self.inner_code()))

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::
            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: latex(C)
            \textnormal{Concatenated code with }[6, 2, 5] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{2^{3}} \textnormal{ as outer code and }[7, 3]\textnormal{ Linear code over }\Bold{F}_{2} \textnormal{ as inner code}
        """
        return "\\textnormal{Concatenated code with }%s \\textnormal{ as outer code and }%s \\textnormal{ as inner code}"\
                % (self.outer_code()._latex_(), self.inner_code()._latex_())

    def minimum_distance(self):
        r"""
        Return the minimum distance of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C.minimum_distance() # long time
            20
        """
        return self.inner_code().minimum_distance() * self.outer_code().minimum_distance()

    def dimension(self):
        """
        Return the dimension of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C.dimension()
            6
        """
        return self.inner_code().dimension() * self.outer_code().dimension()

    def length(self):
        r"""
        Return the length of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C.length()
            42
        """
        return self.inner_code().length() * self.outer_code().length()

    def inner_code(self):
        r"""
        Return the inner code of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C.inner_code()
            Linear code of length 7, dimension 3 over Finite Field of size 2
        """
        return self._inner_code

    def outer_code(self):
        r"""
        Return the dimension of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: C.outer_code()
            [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3
        """
        return self._outer_code










class EncoderConcatenatedCodeOuterField(Encoder):
    r"""
    Construct an encoder for Concatenated codes. This encoder encodes words of
    the outer field (base field of the outer coder) to words of the concatenated
    code.

    INPUT:

    - ``code`` -- The associated code of this encoder.
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: E = EncoderConcatenatedCodeOuterField(C)
            sage: E
            Concatenated code by outer field Encoder for the Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code
        """
        self._initialize_parent(code)

    def __eq__(self, other):
        r"""
        Test equality of EncoderConcatenatedCodeOuterField objects.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: E1 = EncoderConcatenatedCodeOuterField(C)
            sage: E2 = EncoderConcatenatedCodeOuterField(C)
            sage: E1.__eq__(E2)
            True

        """
        return isinstance(other, EncoderConcatenatedCodeOuterField)\
                and self.code() == other.code()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: E = EncoderConcatenatedCodeOuterField(C)
            sage: E
            Concatenated code by outer field Encoder for the Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code

        """
        return "Concatenated code by outer field Encoder for the %s"\
                % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: E = EncoderConcatenatedCodeOuterField(C)
            sage: latex(E)
            \textnormal{Encoder on the outer Field for the }\textnormal{Concatenated code with }[6, 2, 5] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{2^{3}} \textnormal{ as outer code and }[7, 3]\textnormal{ Linear code over }\Bold{F}_{2} \textnormal{ as inner code}
        """
        return "\\textnormal{Encoder on the outer Field for the }%s" % self.code()._latex_()

    def encode(self, word):
        r"""
        Encode ``word`` to a codeword in associated code of ``self``.

        INPUT:

        - ``word`` -- A vector from the message space of the outer code of
          the associated code of ``self``

        OUTPUT:

        - A codeword in the associated code of ``self``

        EXAMPLES::


            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: word = vector((Cout.base_field().gen(), 1))
            sage: E = EncoderConcatenatedCodeOuterField(C)
            sage: E.encode(word)
            (0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1)
        """
        F = self.code().inner_code().base_field()
        dimension = self.code().inner_code().dimension()
        z = self.code().outer_code().encode(word)
        u = []
        for i in z:
            tmp = i.polynomial().list()
            if len(tmp) != dimension:
                while len(tmp) != dimension:
                    tmp.append(F.zero())
            u.append(tmp)
        v = []
        for i in range(0, len(u)):
            v = v + self.code().inner_code().encode(u[i]).list()

        v = vector(v)
        return v

    def unencode_nocheck(self, r, inner_encoder=None, outer_encoder=None):


        C = self.code()
        Cin = C.inner_code()
        Cout = C.outer_code()
        n = Cin.length()
        N = Cout.length()
        u=[]
        for i in range (0, N):
            tmp = vector(Cin.base_field(), r[i*n:(i+1)*n])
            unenc = Cin.unencode(tmp, inner_encoder)
            u.append(unenc)

        u = vector(Cout.base_field(), u)
        u = Cout.unencode(u, outer_encoder)
        return u

    def message_space(self):
        r"""
        Return the message space of ``self``

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: E = EncoderConcatenatedCodeOuterField(C)
            sage: E.message_space()
            Vector space of dimension 2 over Finite Field in x of size 2^3
        """
        return self.code().outer_code().base_field() ** self.code().outer_code().dimension()










class DecoderConcatenatedCodeInnerAndOuterDecoding(Decoder):
    r"""
    Construct a decoder for concatenated codes. This decoder will use
    decoding algorithms from the inner code and the outer code.

    INPUT:

    - ``code`` -- A code associated to this decoder.

    - ``inner_decoder`` -- (default: ``None``) the decoder used by the inner code.
      Default decoder will be used is default value is kept.

    - ``outer_decoder`` -- (default: ``None``) the decoder used by the outer code.
      Default decoder will be used is default value is kept.

    .. NOTE::

        ``inner_decoder`` and ``outer_decoder`` can be:

            - the name of the decoder as a string. In this case, it will be created, saved and set as a
              parameter of ``self``

            - the decoder object itself. In this case, it will be set as a parameter of ``self``. It is
              the only possibility to pass a decoder with optional arguments.

    EXAMPLES::

        sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
        sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
        sage: C = ConcatenatedCode(Cin, Cout)
        sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
        sage: D
        Inner and outer code decoder for the Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code

    """

    def __init__(self, code, inner_decoder=None, outer_decoder=None):
        r"""
        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D
            Inner and outer code decoder for the Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code

        """
        self._inner_decoder = self._set_subdecoder(inner_decoder, code.inner_code().decoder)
        self._outer_decoder = self._set_subdecoder(outer_decoder, code.outer_code().decoder)

        dec_type = self._inner_decoder.decoder_type() & self._outer_decoder.decoder_type()
        self._initialize_parent(code, code.ambient_space(), "OuterField", dec_type)

    def __eq__(self, other):
        r"""
        Test equality of DecoderConcatenatedCodeInnerAndOuterDecoding objects.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D1 = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D2 = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D1 == D2
            True
        """
        return isinstance(other, DecoderConcatenatedCodeInnerAndOuterDecoding) \
                and self.code() == other.code()\
                and self.input_space() == other.input_space()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7], 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D
            Inner and outer code decoder for the Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code
        """
        return "Inner and outer code decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: latex(D)
            Inner and outer code decoder for the Concatenated code with [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3 as outer code and Linear code of length 7, dimension 3 over Finite Field of size 2 as inner code
        """
        return "Inner and outer code decoder for the %s" % self.code()

    def _set_subdecoder(self, subdec_name, subdec_func):
        r"""
        Return a decoder object.

        This is a helper function, used in the constructor of ``self`` to set
        ``inner_decoder`` and ``outer_decoder``.

        INPUT:

        - ``subdec_name`` -- the name of the decoder. It can be either a decoder object, or the name of
          a decoder as a string

        - ``subdec_func`` -- the decoder creation function for the output object

        OUTPUT:

        - a decoder object

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D._set_subdecoder("BerlekampWelch", D.code().outer_code().decoder)
            Berlekamp-Welch decoder for the [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3

        """
        if subdec_name is None:
            return subdec_func()
        elif isinstance(subdec_name, str):
            return subdec_func(subdec_name)
        elif isinstance(subdec_name, Decoder):
            return subdec_name
        else:
            raise TypeError("inner_decoder and outer_decoder must be either a string or a Decoder object")



    def _decode_common(self, r):
        r"""
        Return ``r`` decoded to the message space of inner code of ``self``.

        This method contains code used by both :meth:``decode_to_code`` and
        :meth:``decode_to_message``. It should not be called as is by the
        concatenated code.

        INPUT:

        - ``r`` -- a codeword of ``self``.

        OUTPUT:

        - a vector in the message space of the inner code of ``self``

        EXAMPLES::

            TODO
        """

        C = self.code()
        Dout = self.outer_decoder()
        Din = self.inner_decoder()
        Cin = C.inner_code()
        Cout = C.outer_code()
        n = Cin.length()
        N = Cout.length()
        k = Cin.dimension()
        u=[]
        era = vector(GF(2), N)
        #era is always built. Will be better if only created if used afterwards
        for i in range (0, N):
            tmp = (r[i*n:(i+1)*n])
            try:
                if Din.message_space() == VectorSpace(Cin.base_field(), k):
                    dec = Din.decode_to_message(tmp)
                else:
                    dec = Din.decode_to_code(tmp)
                    dec = Cin.unencode(dec)
            except DecodingFailure:
                dec = vector(Cin.base_ring(), k)
                if "error-erasure" in Dout.decoder_type():
                    for i in range(i*k, (i+1)*k):
                        era[i] = 1
            u.append(dec)

        if "error-erasure" in Dout.decoder_type():
            u = (vector(Cout.base_field(), u), era)
        else:
            u = vector(Cout.base_field(), u)
        return u

    def decode_to_message(self, r, inner_decoder=None, outer_decoder=None):
        r"""
        Decode ``r`` to an element in message space of ``self``.

        This decoding algorithm works as follows:

        1. it splits ``r`` into vectors of the same length as the inner code
        2. it uses ``inner_decoder`` on every vector got after the first step
        3. if a vector is impossible to decode, and ``outer_decoder`` is
           an error-erasure decoder, this vector is fully erased. If ``outer_decoder``
           is not an error-erasure decoder, the vector is set to 0
        4. it concatenates all vectors got after the third step in one vector
        5. it uses ``outer_decoder`` on the vector got after the fourth step

        INPUT:

        - ``r`` -- a codeword of ``self``.
        - ``inner_decoder`` -- (default: ``None``) the decoder used by the inner code.
          Default decoder will be used is default value is kept.
        - ``outer_decoder`` -- (default: ``None``) the decoder used by the outer code.
          Default decoder will be used is default value is kept.

        OUTPUT:

        - a vector of the message space of ``self``.

        EXAMPLES::

            TODO
        """
        msg_sp = self.message_space()
        u = self._decode_common(r)
        if self.outer_decoder().message_space() == msg_sp:
            ret = self.outer_decoder().decode_to_message(u)
        else:
            ret = self.outer_decoder().decode_to_code(u)
            ret = self.code().outer_code().unencode(ret)
        return ret

    def decode_to_code(self, r):
        r"""
        Decode ``r`` to a codeword of ``self``.

        See :meth: ``decode_to_message`` for a description of the decoding algorithm.

        INPUT:

        - ``r`` -- a codeword of ``self``.

        OUTPUT:

        - a vector of ``self``.

        EXAMPLES::

            TODO

        """
        u = self._decode_common(r)
        ret = self.outer_decoder().decode_to_code(u)
        v = []
        for i in range(0, len(ret)):
            v = v + self.code().inner_code().encode(ret[i]).list()

        v = vector(v)

        return v

    def inner_decoder(self):
        r"""
        Return the chosen decoder for the inner code of the associated code of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D.inner_decoder()
            Syndrome decoder for the Linear code of length 7, dimension 3 over Finite Field of size 2
        """
        return self._inner_decoder

    def outer_decoder(self):
        r"""
        Return the chosen decoder for the outer code of the associated code of ``self``.

        EXAMPLES::

            sage: Cin = QAryHammingCode(GF(2), 3).dual_code()
            sage: Cout = GeneralizedReedSolomonCode(GF(2^3,'x').list()[1:7] , 2)
            sage: C = ConcatenatedCode(Cin, Cout)
            sage: D = DecoderConcatenatedCodeInnerAndOuterDecoding(C)
            sage: D.outer_decoder()
            Gao decoder for the [6, 2, 5] Generalized Reed-Solomon Code over Finite Field in x of size 2^3
        """

        return self._outer_decoder
