r"""
Decoder

Representation of an error-correction algorithm for a code.
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.misc.abstract_method import abstract_method

class Decoder(object):
    r"""
    Abstract top-class for Decoder objects.
    """

    def _initialize_parent(self, code, input_space, connected_encoder_name,\
        decoder_type):
        r"""
        Initialize some parameters for a Decoder object.

        This is a private method, which should be called by the constructor
        of every decoder, as it automatically initializes the mandatory
        parameters of a Decoder object.

        INPUT:

        - ``code`` -- the associated code of ``self``

        - ``input_space`` -- the input space of ``self``

        - ``connected_encoder_name`` -- the associated encoder, which will be
          used by default by ``self``

        - ``decoder_type`` -- the set of types of ``self``. Describes the
          behaviour of ``self``.

        EXAMPLES:

        We first create a new Decoder subclass
        ::

            sage: class DecoderExample(sage.coding.decoder.Decoder):
            ....:   def __init__(self, code):
            ....:       in_space = code.base_field()
            ....:       connected_enc = "EvaluationPolynomial"
            ....:       decoder_type = {"unique", "always-succeed", "is_example"}
            ....:       self._initialize_parent(code, in_space, connected_enc, decoder_type)

        We now create a member of our brand new class
        ::

            sage: C = GeneralizedReedSolomonCode(GF(59).list()[:40], 12)
            sage: D = DecoderExample(C)

        We can check its parameters
        ::

            sage: D.input_space()
            Finite Field of size 59
            sage: D.decoder_type()
            {'always-succeed', 'is_example', 'unique'}
            sage: D.connected_encoder()
            Evaluation polynomial-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
            sage: D.code()
            [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._code = code
        self._input_space = input_space
        self._connected_encoder_name = connected_encoder_name
        self._decoder_type = decoder_type

    def decoder_type(self):
        r"""
        Return the set of types of ``self``.
        These types give some clues on the nature of ``self``
        and its decoding algorithm.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: D = C.decoder()
            sage: D.decoder_type()
            {'always-succeed', 'hard-decision', 'unique'}
        """
        return self._decoder_type

    def decode_to_code(self, r):
        r"""
        Correct the errors in word and returns a codeword.

        This is a base method for Decoder base class. It
        assumes that the method ``decode_to_message()`` has
        been implemented, else it returns an exception.

        INPUT:

        - ``r`` -- a vector of the same length as ``self`` over the
          base field of ``self``

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: word = vector((5, 6, 5, 9, 3, 5, 0, 6, 8, 2))
            sage: w_err = word + vector((0, 1, 0, 0, 0, 1, 0, 0, 0, 0))
            sage: C.decode_to_code(word)
            (5, 6, 5, 9, 3, 5, 0, 6, 8, 2)
        """
        if hasattr(self, "defaulting_decode_to_message"):
            raise NotImplementedError
        else:
            word = self.decode_to_message(r)
            return self.connected_encoder().encode(word)

    def connected_encoder(self):
        r"""
        Return the connected encoder of ``self``

        EXAMPLES
        ::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: D = C.decoder()
            sage: D.connected_encoder()
            Evaluation polynomial-style encoder for the [10, 4, 7] Generalized Reed-Solomon Code over Finite Field of size 11
        """
        return self.code().encoder(name=self._connected_encoder_name)

    def decode_to_message(self, r):
        r"""
        Correct the errors in word and decodes it to the message space.

        This is a base method for Decoder base class. It
        assumes that the method ``decode_to_code()`` has
        been implemented, else it returns an exception.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: word = (5, 6, 5, 9, 3, 5, 0, 6, 8, 2)
            sage: C.decode_to_message(word)
            (5, 8, 1, 3)
        """
        self.defaulting_decode_to_message = True
        return self.code().unencode(self.decode_to_code(r))

    def code(self):
        r"""
        Return the associated code of ``self``.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: D = C.decoder()
            sage: D.code()
            [10, 4, 7] Generalized Reed-Solomon Code over Finite Field of size 11
        """
        return self._code

    def message_space(self):
        r"""
        Return the message space of connected encoder of ``self``.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: D = C.decoder()
            sage: D.message_space()
            Univariate Polynomial Ring in x over Finite Field of size 11
        """
        return self.connected_encoder().message_space()

    def input_space(self):
        r"""
        Return the input space of ``self``.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: D = C.decoder()
            sage: D.input_space()
            Vector space of dimension 10 over Finite Field of size 11
        """
        if hasattr(self, "_input_space"):
            return self._input_space
        else:
            raise NotImplementedError("Decoder does not have an _input_space parameter")

    @abstract_method(optional = True)
    def decoding_radius(self, **kwargs):
        r"""
        Returns the maximal number of errors that ``self`` is able to correct.

        This is an abstract method and should be implemented in subclasses.
        """
        raise NotImplementedError

class DecodingFailure(Exception):
    pass
