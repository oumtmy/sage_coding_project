r"""
Encoder

Given a code, an encoder embeds some specific methods to link this code's
message space to this code's ambient space.
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.modules.free_module_element import vector
from sage.misc.abstract_method import abstract_method
from sage.misc.cachefunc import cached_method

class Encoder(object):
    r"""
    Abstract top-class for Encoder objects.
    """

    def _initialize_parent(self, code):
        r"""
        Initialize some parameters for an Encoder object.

        This is a private method, which should be called by the constructor
        of every encoder, as it automatically initializes the mandatory
        parameters of an Encoder object.

        INPUT:

        - ``code`` -- the associated code of ``self``

        EXAMPLES:

        We first create a new Encoder subclass
        ::

            sage: class EncoderExample(sage.coding.encoder.Encoder):
            ....:   def __init__(self, code):
            ....:       self._initialize_parent(code)

        We now create a member of our newly made class
        ::

            sage: C = GeneralizedReedSolomonCode(GF(59).list()[:40], 12)
            sage: E = EncoderExample(C)            

        We can check its parameters
        ::

            sage: E.code()
            [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._code = code

    def encode(self, word):
        r"""
        Encode ``word`` as a codeword of ``self``.

        This is a default implementation which assumes that the message
        space of the encoder is a Vector Space. If this is not the case,
        this method should be overwritten by the subclass.
        INPUT:

        - ``word`` -- a vector of the same length as dimension of ``self``

        OUTPUT:

        - a vector of ``self``

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: word = vector((47, 9, 126, 43)) 
            sage: C.encode(word)
            (3, 5, 0, 4, 0, 4, 10, 1, 4, 2)
        """
        return vector(word) * self.generator_matrix()

    def unencode(self, c, nocheck=False, **kwargs):
        r"""
        Return ``c`` decoded to the message space of ``self``.

        INPUT:

        - ``c`` -- a vector of the same length as ``self`` over the
          base field of ``self``

        - ``nocheck`` -- (default: ``False``) checks if ``c`` is in self. If this is set
          to True, the return value of this method is not guaranteed to be correct.
        
        OUTPUT:

        - a vector of the message of ``self`` 

        EXAMPLES
        ::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: word = vector((5, 6, 5, 9, 3, 5, 0, 6, 8, 2)) 
            sage: C.unencode(word)
            (5, 8, 1, 3)
        """
        if nocheck == False:
            if c not in self.code():
                raise EncodingFailure("Given word is not in the code")
            else:
                return self.unencode_nocheck(c, **kwargs)
        else:
            return self.unencode_nocheck(c, **kwargs)

    @cached_method
    def _unencoder_matrix(self):
        r"""
        Find an information set for G, and return the inverse of those
        columns of G.

        This method was written by Johan Nielsen as a part of codinglib.
        See : https://bitbucket.org/jsrn/codinglib

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: E = C.encoder()
            sage: E._unencoder_matrix()
            [1 0 1 9]
            [0 3 3 6]
            [0 4 2 5]
            [0 4 5 2]
        """
        Gt = self.generator_matrix().matrix_from_columns(self.code().information_set())
        return Gt.inverse()

    def unencode_nocheck(self, c, **kwargs):
        r"""
        Return the message corresponding to a codeword. 
        
        When c is not a codeword, the output is unspecified.

        This method was written by Johan Nielsen as a part of codinglib.
        See : https://bitbucket.org/jsrn/codinglib

        INPUT:

        - ``c`` -- a vector of the same length as ``self`` over the
          base field of ``self``

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: word = vector((5, 6, 5, 9, 3, 5, 0, 6, 8, 2)) 
            sage: E = C.encoder()
            sage: E.unencode_nocheck(word)
            (5, 8, 1, 3)
        """
        U = self._unencoder_matrix()
        info_set = self.code().information_set()
        cc = vector( c[i] for i in info_set )
        return cc * U

    def code(self):
        r"""
        Return the code in which ``self.encode()`` has its output.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: E = C.encoder()
            sage: E.code()
            [10, 4, 7] Generalized Reed-Solomon Code over Finite Field of size 11
        """
        return self._code

    def message_space(self):
        r"""
        Returns the ambient space of allowed input to ``self.encode()``.
        Note that the ``self.encode()`` is possibly a partial function over
        the ambient space.

        EXAMPLES::

            sage: C = GeneralizedReedSolomonCode(GF(11).list()[:10], 4)
            sage: E = C.encoder()
            sage: E.message_space()
            Vector space of dimension 4 over Finite Field of size 11
        """
        return self.code().base_field()**(self.code().dimension())
    
    @abstract_method(optional = True)
    def generator_matrix(self):
        r"""
        Return a generator matrix of the associated code of self.

        This is an abstract method and it should be implemented separately.
        Reimplementing this for each subclass of Encoder is not mandatory 
        (as encoders with a polynomial message space, for instance, do not
        need a generator matrix).
        """
        raise NotImplementedError

class EncodingFailure(Exception):
    pass
