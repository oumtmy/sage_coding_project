r"""
Generalized Reed-Solomon Code

Given `n` different evaluation points `\alpha_1, \dots, alpha_n` from some
finite field `F`, and `n` column multipliers `\beta_1, \dots, \beta_n`, the
corresponding GRS code of dimension `k` is the set:

.. math::

    \{ (\beta_1 f(\alpha_1), \ldots, \beta_n f(\alpha_n)  \mid  f \in F[x], \deg f < k \}

This file contains the following elements:

    - *GeneralizedReedSolomonCode*, the class for GRS codes
    - *EncoderGRSEvaluationVector*, an encoder with a vectorial message space
    - *EncoderGRSEvaluationPolynomial*, an encoder with a polynomial message space
    - *DecoderGRSBerlekampWelch*, a decoder based on the Berlekamp-Welch decoding algorithm
    - *DecoderGRSGao*, a decoder based on the Gao decoding algorithm
    - *DecoderGRSErrorErasure*, a decoder able to correct both errors and erasures
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.matrix.constructor import matrix
from sage.rings.finite_rings.constructor import GF
from sage.combinat.cartesian_product import CartesianProduct
from sage.modules.free_module_element import vector
from sage.modules.free_module import VectorSpace
from sage.rings.integer import Integer
from sage.misc.cachefunc import cached_method
from copy import copy
from weak_popov_form import *
from linear_code import AbstractLinearCode
from decoder import Decoder, DecodingFailure
from encoder import Encoder

class GeneralizedReedSolomonCode(AbstractLinearCode):
    r"""
    Construct a Generalized Reed-Solomon code.


    INPUT:

    - ``evaluation_points`` -- A list of evaluation points in a finite field F

    - ``dimension`` -- The dimension of the code

    - ``column_multipliers`` -- (default: ``None``) List of column multipliers in F for this code.
      All column multipliers are set to 1 if default value is kept.

    EXAMPLES:

    We construct a GRS code with a manually built support, without specifying column multipliers
    ::

        sage: F = GF(7)
        sage: support = [F(i) for i in range(1,7)]
        sage: C = GeneralizedReedSolomonCode(support,3)
        sage: C
        [6, 3, 4] Generalized Reed-Solomon Code over Finite Field of size 7


    We construct a GRS code without specifying column multipliers
    ::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: C
        [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59

    It is also possible to specify the column multipliers
    ::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k, F.list()[1:n+1])
        sage: C
        [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """
    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, evaluation_points, dimension, column_multipliers=None):
        r"""
        TESTS:

        If the evaluation points are not from a finite field, it raises an error
        ::

            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(list()[:n], k)
            Traceback (most recent call last):
            ...
            ValueError: Evaluation points must be in a finite field

        If the column multipliers are not from a finite field, or not in the same
        finite field as the evaluation points, it raises an error
        ::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k, list()[1:n+1])
            Traceback (most recent call last):
            ...
            ValueError: Column multipliers must be in a finite field

            sage: F2 = GF(61)
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k, F2.list()[1:n+1])
            Traceback (most recent call last):
            ...
            ValueError: Column multipliers and evaluation points must be in the same field

        The number of column multipliers is checked as well
        ::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k, F.list()[1:n])
            Traceback (most recent call last):
            ...
            ValueError: There must be exactly 40 column multipliers

        It is not allowed to have 0 as a column multiplier
        ::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k, F.list()[:n])
            Traceback (most recent call last):
            ...
            ValueError: All column multipliers must be non-zero

        And all the evaluation points must be different
        ::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode([F.one()]*n, k)
            Traceback (most recent call last):
            ...
            ValueError: All evaluation points must be different
        """
        F = vector(evaluation_points).base_ring()
        if F.is_finite() == False:
            raise ValueError("Evaluation points must be in a finite field")
        super(GeneralizedReedSolomonCode, self).__init__(F, len(evaluation_points))
        self._dimension = dimension
        self._evaluation_points = copy(evaluation_points)
        self._encoder_default_name = "EvaluationVector"
        self._decoder_default_name = "Gao"

        if column_multipliers is None:
            self._column_multipliers = [self.base_field().one()] * self._length
        else:
            Fc = vector(column_multipliers).base_ring()
            if Fc.is_finite() == False:
                raise ValueError("Column multipliers must be in a finite field")
            elif Fc != self.base_field():
                raise ValueError("Column multipliers and evaluation points\
                        must be in the same field")
            self._column_multipliers = copy(column_multipliers)
        if len(self._column_multipliers) != self._length:
            raise ValueError("There must be exactly %s column multipliers"\
                    % self._length)
        if 0 in self._column_multipliers:
            raise ValueError("All column multipliers must be non-zero")
        if len(self._evaluation_points) != len(set(self._evaluation_points)):
            raise ValueError("All evaluation points must be different")

    def __eq__(self, other):
        r"""
        Test equality of Generalized Reed-Solomon Code objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C1 = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: C2 = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: C1.__eq__(C2)
            True
        """
        return isinstance(other, GeneralizedReedSolomonCode) \
                and self.base_field() == other.base_field() \
                and self.length() == other.length() \
                and self.dimension() == other.dimension() \
                and self.evaluation_points() == other.evaluation_points() \
                and self.column_multipliers() == other.column_multipliers()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: C
            [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "[%s, %s, %s] Generalized Reed-Solomon Code over %s"\
                % (self.length(), self.dimension(),\
                self.minimum_distance(), self.base_field())

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: latex(C)
            [40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "[%s, %s, %s] \\textnormal{ Generalized Reed-Solomon Code over } %s"\
                % (self.length(), self.dimension() ,self.minimum_distance(),\
                self.base_field()._latex_())

    def minimum_distance(self):
        r"""
        Return the minimum distance of ``self``. Since a GRS code is always MDS,
        this always returns ``C.length() - C.dimension() + 1``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: C.minimum_distance()
            29
        """
        return self.length() - self.dimension() + 1

    def evaluation_points(self):
        r"""
        Return the list of evaluation points of ``self``.

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: C.evaluation_points()
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        """
        return self._evaluation_points

    def column_multipliers(self):
        r"""
        Return the list of column multipliers of ``self``.

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: C.column_multipliers()
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        """
        return self._column_multipliers










class EncoderGRSEvaluationVector(Encoder):
    r"""
    Construct an encoder for GRS codes. This encoder can encode vectors to codewords.

    INPUT:

    - ``code`` -- The associated code of this encoder.

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: E = EncoderGRSEvaluationVector(C)
        sage: E
        Evaluation vector-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationVector(C)
            sage: E
            Evaluation vector-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code)
        self._R = code.base_field()['x']

    def __eq__(self, other):
        r"""
        Test equality of EncoderGRSEvaluationVector objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D1 = EncoderGRSEvaluationVector(C)
            sage: D2 = EncoderGRSEvaluationVector(C)
            sage: D1.__eq__(D2)
            True
        """
        return isinstance(other, EncoderGRSEvaluationVector) \
                and self.code() == other.code()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationVector(C)
            sage: E
            Evaluation vector-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Evaluation vector-style encoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationVector(C)
            sage: latex(E)
            \textnormal{Evaluation vector-style encoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Evaluation vector-style encoder for the }%s" % self.code()._latex_()

    @cached_method
    def generator_matrix(self):
        r"""
        Return a generator matrix of ``self``

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationVector(C)
            sage: E.generator_matrix()
            [1 1 1 1 1 1 1 1 1 1]
            [0 1 2 3 4 5 6 7 8 9]
            [0 1 4 9 5 3 3 5 9 4]
            [0 1 8 5 9 4 7 2 6 3]
            [0 1 5 4 3 9 9 3 4 5]
        """
        base_field = self.code().base_field()
        dimension = self.code().dimension()
        length = self.code().length()
        alphas = self.code().evaluation_points()
        col_mults = self.code().column_multipliers()
        return matrix(base_field, dimension, length, lambda i,j : col_mults[j]*alphas[j]**i)

    def unencode_nocheck(self, c):
        r"""
        Unencode ``c`` to an element in message space of ``self``.
        Does not check if ``c`` belongs to the code.

        INPUT:

        - ``c`` -- A vector with the same length as the code

        OUTPUT:

        - An element of the message space

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10 , 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationVector(C)
            sage: c = vector(F, (10, 3, 9, 6, 5, 6, 9, 3, 10, 8))
            sage: E.unencode_nocheck(c)
            (10, 3, 1, 0, 0)
        """
        C = self.code()
        alphas = self.code().evaluation_points()
        col_mults = self.code().column_multipliers()
        length = self.code().length()
        dimension = self.code().dimension()

        c = [c[i]/col_mults[i] for i in range(length)]
        points = [(alphas[i], c[i]) for i in range(dimension)]

        Pc = self._R.lagrange_polynomial(points).list()
        Pc = Pc + [self.code().base_field().zero()]*(dimension - len(Pc))

        m = vector(self.code().base_field(), Pc)
        return m










class EncoderGRSEvaluationPolynomial(Encoder):
    r"""
    Construct an encoder for GRS codes. This encoder can encode polynomials to codewords.

    INPUT:

    - ``code`` -- The associated code of this encoder.

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: E = EncoderGRSEvaluationPolynomial(C)
        sage: E
        Evaluation polynomial-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationPolynomial(C)
            sage: E
            Evaluation polynomial-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """

        self._initialize_parent(code)
        self._R = code.base_field()['x']

    def __eq__(self, other):
        r"""
        Test equality of EncoderGRSEvaluationPolynomial objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D1 = EncoderGRSEvaluationPolynomial(C)
            sage: D2 = EncoderGRSEvaluationPolynomial(C)
            sage: D1.__eq__(D2)
            True
        """
        return isinstance(other, EncoderGRSEvaluationPolynomial) \
                and self.code() == other.code()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationPolynomial(C)
            sage: E
            Evaluation polynomial-style encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Evaluation polynomial-style encoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationPolynomial(C)
            sage: latex(E)
            \textnormal{Evaluation polynomial-style encoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Evaluation polynomial-style encoder for the }%s" % self.code()._latex_()

    def encode(self, p):
        r"""
        Encode ``p`` to a codeword in associated code of ``self``

        INPUT:

        - ``p`` -- A polynomial from ``self`` message space

        OUTPUT:

        - A codeword in associated code of ``self``

        EXAMPLES::

            sage: F = GF(11)
            sage: K.<x>=F[]
            sage: n, k = 10 , 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationPolynomial(C)
            sage: p = x^2 + 3*x + 10
            sage: E.encode(p)
            (10, 3, 9, 6, 5, 6, 9, 3, 10, 8)
        """
        alphas    = self.code().evaluation_points()
        col_mults = self.code().column_multipliers()
        field     = self.code().base_ring()
        length    = self.code().length()
        u = vector(field, [col_mults[i]*p(alphas[i]) for i in range(length)])
        return u

    def unencode_nocheck(self, c):
        r"""
        Unencode ``c`` to an element in message space of ``self``.
        Does not check if ``c`` belongs to the code.

        INPUT:

        - ``c`` -- A vector with the same length as the code

        OUTPUT:

        - An element of the message space

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10 , 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationPolynomial(C)
            sage: c = vector(F, (10, 3, 9, 6, 5, 6, 9, 3, 10, 8))
            sage: E.unencode_nocheck(c)
            x^2 + 3*x + 10
        """

        alphas = self.code().evaluation_points()
        col_mults = self.code().column_multipliers()
        length = self.code().length()
        dimension = self.code().dimension()

        c = [c[i]/col_mults[i] for i in range(length)]
        points = [(alphas[i], c[i]) for i in range(dimension)]

        Pc = self._R.lagrange_polynomial(points)
        return Pc

    def message_space(self):
        r"""
        Return the message space of ``self``

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10 , 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderGRSEvaluationPolynomial(C)
            sage: E.message_space()
            Univariate Polynomial Ring in x over Finite Field of size 11
        """
        return self._R










class DecoderGRSBerlekampWelch(Decoder):
    r"""
    Construct a decoder for GRS codes. This decoder will use the Berlekamp-Welch decoding algorithm.

    INPUT:

    - ``code`` -- A code associated to this decoder

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: D = DecoderGRSBerlekampWelch(C)
        sage: D
        Berlekamp-Welch decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSBerlekampWelch(C)
            sage: D
            Berlekamp-Welch decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code, code.ambient_space(),\
                "EvaluationPolynomial",\
                {"hard-decision", "unique", "always-succeed"})

    def __eq__(self, other):
        r"""
        Test equality of DecoderGRSBerlekampWelch objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D1 = DecoderGRSBerlekampWelch(C)
            sage: D2 = DecoderGRSBerlekampWelch(C)
            sage: D1.__eq__(D2)
            True
        """
        return isinstance(other, DecoderGRSBerlekampWelch) \
                and self.code() == other.code()\
                and self.input_space() == other.input_space()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSBerlekampWelch(C)
            sage: D
            Berlekamp-Welch decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Berlekamp-Welch decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSBerlekampWelch(C)
            sage: latex(D)
            \textnormal{Berlekamp Welch decoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Berlekamp Welch decoder for the }%s"\
                % self.code()._latex_()

    def decode_to_message(self, r):
        r"""
        Decode ``r`` to an element in message space of ``self``

        INPUT:

        - ``r`` -- a codeword of ``self``

        OUTPUT:

        - a vector of ``self`` message space

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSBerlekampWelch(C)
            sage: r = vector(F, (8, 2, 6, 10, 6, 10, 7, 6, 7, 1))
            sage: D.decode_to_message(r)
            x^4 + 7*x^3 + 10*x^2 + 9*x + 8

        If we try to decode a word with too many errors, it returns
        an exception::

            sage: r[0] = r[1] = r[2] = 3
            sage: D.decode_to_message(r)
            Traceback (most recent call last):
            ...
            DecodingFailure
        """
        C = self.code()
        col_mults = C.column_multipliers()
        if r in C:
            return self.connected_encoder().unencode_nocheck(r)

        r = [r[i]/col_mults[i] for i in range(0, C.length())]

        t  = (C.minimum_distance()-1) // 2
        l0 = C.length()-1-t
        l1 = C.length()-1-t-(C.dimension()-1)
        S  = matrix(C.base_field(), C.length(), l0+l1+2, lambda i,j :\
                (C.evaluation_points()[i])**j if j<(l0+1)\
                else r[i]*(C.evaluation_points()[i])**(j-(l0+1)))
        S  = S.right_kernel()
        S  = S.basis_matrix().row(0)
        R = C.base_field()['x']

        Q0 = R(S.list_from_positions(xrange(0, l0+1)))
        Q1 = R(S.list_from_positions(xrange(l0+1 , l0+l1+2)))

        if not Q1.divides(Q0):
            raise DecodingFailure()
        f = (-Q0)//Q1

        return f

    def decoding_radius(self):
        r"""
        Return maximal number of errors that ``self`` can decode

        OUTPUT:

        - the number of errors as an integer

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSBerlekampWelch(C)
            sage: D.decoding_radius()
            14
        """
        return (self.code().minimum_distance()-1)//2











class DecoderGRSGao(Decoder):
    r"""
    Construct a decoder for GRS codes. This decoder will use the Gao decoding algorithm.

    INPUT:

    - ``code`` -- The associated code of this decoder.

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: D = DecoderGRSGao(C)
        sage: D
        Gao decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: D
            Gao decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code, code.ambient_space(),\
                "EvaluationPolynomial",\
                {"hard-decision", "unique", "always-succeed"})

    def __eq__(self, other):
        r"""
        Test equality of DecoderGRSGao objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D1 = DecoderGRSGao(C)
            sage: D2 = DecoderGRSGao(C)
            sage: D1.__eq__(D2)
            True
        """
        return isinstance(other, DecoderGRSGao) \
                and self.code() == other.code()\
                and self.input_space() == other.input_space()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: D
            Gao decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Gao decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: latex(D)
            \textnormal{Gao decoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Gao decoder for the }%s" % self.code()._latex_()

    @cached_method
    def precompute(self, PolRing):
        r"""
        Return the unique monic polynomial vanishing on the evaluation points.
        Helper function for internal purposes.

        INPUT:

        - ``PolRing`` -- polynomial ring of the output

        OUTPUT:

        - a polynomial over ``PolRing``

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: P = PolynomialRing(F,'x')
            sage: D.precompute(P)
            x^10 + 10*x^9 + x^8 + 10*x^7 + x^6 + 10*x^5 + x^4 + 10*x^3 + x^2 + 10*x
        """
        alphas = self.code().evaluation_points()
        G = 1
        x = PolRing.gens()[0]
        for i in range(0, self.code().length()):
            G = G*(x-self.code().evaluation_points()[i])
        return G

    def partial_xgcd(self, a, b, PolRing):
        r"""
        Returns the greatest common divisor of ``a`` and ``b``.

        The computation stops whenever the degree of the xgcd falls below
        `\frac{d+k}{2}`, where `d` is the dimension of ``self.code()`` and
        `k` its dimension.

        This is a helper function, used in :meth:`decode_to_message`.

        INPUT:

        - ``a, b`` -- polynomials over ``PolRing``

        - ``PolRing`` -- polynomial ring of the output

        OUTPUT:

        - a tuple of polynomials ``(r, s)`` where ``r`` is to the xgcd of
          ``a`` and ``b`` and ``s`` is the Bezout coefficient of ``a``.

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: P = PolynomialRing(F,'x')
            sage: x = P.parameter()
            sage: a = 5*x^2 + 9*x + 8
            sage: b = 10*x^2 + 3*x + 5
            sage: D.partial_xgcd(a, b, P)
            (10*x^2 + 3*x + 5, 1)
        """
        stop = floor(self.code().dimension() + self.code().length()) / 2
        s = PolRing.one()
        prev_s = PolRing.zero()

        r = b
        prev_r = a
        while(r.degree() >= stop):
            q = prev_r.quo_rem(r)[0]
            (prev_r, r) = (r, prev_r - q * r)
            (prev_s, s) = (s, prev_s - q * s)

        return (r, s)

    def decode_to_message(self, r):
        r"""
        Decodes ``r`` to an element in message space of ``self``

        INPUT:

        - ``r`` -- a codeword of ``self``

        OUTPUT:

        - a vector of ``self`` message space

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: r = vector(F, (8, 2, 6, 10, 6, 10, 7, 6, 7, 1))
            sage: D.decode_to_message(r)
            x^4 + 7*x^3 + 10*x^2 + 9*x + 8

        If we try to decode a word with too many errors, it returns
        an exception::

            sage: r[0] = r[1] = r[2] = 3
            sage: D.decode_to_message(r)
            Traceback (most recent call last):
            ...
            DecodingFailure
        """
        C = self.code()
        alphas = C.evaluation_points()
        col_mults = self.code().column_multipliers()
        PolRing = self.code().base_field()['x']
        G = self.precompute(PolRing)

        if r in C:
            return self.connected_encoder().unencode_nocheck(r)

        points = [(alphas[i], r[i]/col_mults[i]) for i in \
                range(0, self.code().length())]
        R = PolRing.lagrange_polynomial(points)

        (Q1, Q0) = self.partial_xgcd(G, R, PolRing)

        #W = matrix(PolRing, 2, 2, [1, R, 0, G])
        #weights = [self.code().dimension(), Integer(0)]
        #module_apply_weights(W, weights)
        #module_weak_popov(W)

        #i = 0 if W[0, 0].degree() > W[0, 1].degree() else 1
        #module_remove_weights(W, weights)
        #Q0 = W[i, 0]
        #Q1 = W[i, 1]

        if not Q0.divides(Q1):
            raise DecodingFailure()
        h = Q1//Q0

        return h

    def decoding_radius(self):
        r"""
        Return maximal number of errors that ``self`` can decode

        OUTPUT:

        - the number of errors as an integer

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSGao(C)
            sage: D.decoding_radius()
            14
        """
        return (self.code().minimum_distance()-1)//2










class DecoderGRSErrorErasure(Decoder):
    r"""
    Construct a decoder for GRS codes. This decoder is able to correct both errors
    and erasures within its decoding radius.

    INPUT:

    - ``code`` -- The associated code of this decoder.

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: D = DecoderGRSErrorErasure(C)
        sage: D
        Error-Erasure decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSErrorErasure(C)
            sage: D
            Error-Erasure decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        input_space = CartesianProduct(code.ambient_space(),\
                VectorSpace(GF(2), code.ambient_space().dimension()))
        self._initialize_parent(code, input_space, "EvaluationVector",\
                {"error-erasure", "unique", "always-succeed"})

    def __eq__(self, other):
        r"""
        Test equality of DecoderGRSErrorErasure objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D1 = DecoderGRSErrorErasure(C)
            sage: D2 = DecoderGRSErrorErasure(C)
            sage: D1.__eq__(D2)
            True
        """
        return isinstance(other, DecoderGRSErrorErasure) \
                and self.code() == other.code()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSErrorErasure(C)
            sage: D
            Error-Erasure decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Error-Erasure decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSErrorErasure(C)
            sage: latex(D)
            \textnormal{Error-Erasure decoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Error-Erasure decoder for the }%s"\
                % self.code()._latex_()

    def decode_to_message(self, word_and_erasure_vector):
        r"""
        Decode ``word_and_erasure_vector`` to an element in message space
        of ``self``

        INPUT:

        - ``word_and_erasure_vector`` -- a pair of vectors, where
          first element is a codeword of ``self`` and second element
          is a vector of GF(2) containing erasure positions

        OUTPUT:

        - a vector of ``self`` message space

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSErrorErasure(C)
            sage: r = vector(F, (8, 2, 6, 10, 6, 10, 7, 6, 7, 1))
            sage: e = vector(GF(2), (0, 0, 0, 1, 0, 0, 0, 1, 0, 0))
            sage: w_e = (r, e)
            sage: D.decode_to_message(w_e)
            (8, 9, 10, 7, 1)

        If we try to decode a word with too many erasures, it returns
        an exception::

            sage: e = vector(GF(2), (1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
            sage: w_e = (r, e)
            sage: D.decode_to_message(w_e)
            Traceback (most recent call last):
            ...
            DecodingFailure: Too many erasures in the received word
        """

        word, erasure_vector = word_and_erasure_vector
        if erasure_vector.hamming_weight() >= self.code().minimum_distance():
            raise DecodingFailure("Too many erasures in the received word")

        shorten_word = vector([word[i] for i in range(len(word))\
                if erasure_vector[i]!=1])
        C1_length = len(shorten_word)
        C1_evaluation_points = [self.code().evaluation_points()[i] for i in\
                range(self.code().length()) if erasure_vector[i]!=1]
        C1_column_multipliers = [self.code().column_multipliers()[i] for i in\
                range(self.code().length()) if erasure_vector[i]!=1]
        C1 = GeneralizedReedSolomonCode(C1_evaluation_points,\
                self.code().dimension(), C1_column_multipliers)
        return C1.decode_to_message(shorten_word)

    def decoding_radius(self, number_erasures):
        r"""
        Return maximal number of errors that ``self`` can decode according
        to how many erasures it receives

        INPUT:

        - ``number_erasures`` -- the number of erasures when we try to decode

        OUTPUT:

        - the number of errors as an integer

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderGRSErrorErasure(C)
            sage: D.decoding_radius(5)
            12

        If we receive too many erasures, it returns an exception as codeword will
        be impossible to decode::

            sage: D.decoding_radius(30)
            Traceback (most recent call last):
            ...
            ValueError: The number of erasures exceed decoding capability
        """
        diff = self.code().minimum_distance() - number_erasures
        if diff <= 0:
            raise ValueError("The number of erasures exceed decoding capability")
        else :
            return diff // 2
