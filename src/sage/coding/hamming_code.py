r"""
Hamming Code
"""
from linear_code import AbstractLinearCode
from sage.matrix.matrix_space import MatrixSpace
from sage.schemes.projective.projective_space import ProjectiveSpace
from sage.rings.integer import Integer


class QAryHammingCode(AbstractLinearCode):
    r"""
    Construct a Q-ary Hamming Code.

    INPUT:

    - ``base_field`` -- the base field over which ``self`` is defined

    - ``order`` -- the order of ``self``.
    """
    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, base_field, order):
        r"""
        EXAMPLES::

            sage: C = QAryHammingCode(GF(7), 3)
            sage: C
            [57 54] Q-ary Hamming Code
        """
        q = base_field.order()
        length = Integer((q ** order - 1) / (q - 1))
        super(QAryHammingCode, self).__init__(base_field, length)
        self._dimension = length - order
        self._encoder_default_name = "ParityCheck"
        self._decoder_default_name = "Syndrome"

    def __eq__(self, other):
        r"""
        Test equality of Hamming codes objects.

        EXAMPLES::

            sage: C1 = QAryHammingCode(GF(7), 3)
            sage: C2 = QAryHammingCode(GF(7), 3)
            sage: C1 == C2
            True
        """
        return isinstance(other, QAryHammingCode)\
                and self.length() == other.length()\
                and self.dimension() == other.dimension()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: C = QAryHammingCode(GF(7), 3)
            sage: C
            [57 54] Q-ary Hamming Code
        """
        return "[%s %s] Q-ary Hamming Code" % (self.length(), self.dimension())

    def _latex_(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: C = QAryHammingCode(GF(7), 3)
            sage: latex(C)
            [57 54] \textnormal{ Q-ary Hamming Code}
        """
        return "[%s %s] \\textnormal{ Q-ary Hamming Code}" % (self.length(), self.dimension())


    def parity_check_matrix(self):
        r"""
        Returns a parity check matrix of ``self``.

        EXAMPLES::

            sage: C = QAryHammingCode(GF(3), 3)
            sage: C.parity_check_matrix()
            [1 0 1 1 0 1 0 1 1 1 0 1 1]
            [0 1 1 2 0 0 1 1 2 0 1 1 2]
            [0 0 0 0 1 1 1 1 1 2 2 2 2]
        """
        n = self.length()
        F = self.base_field()
        m = n - self.dimension()
        MS = MatrixSpace(F,n,m)
        X = ProjectiveSpace(m-1,F)
        PFn = [list(p) for p in X.point_set(F).points(F)]

        H = MS(PFn).transpose()
        if m % 2 == 0:
            for i in range(m//2):
                H.swap_rows(i, m-1)
        else:
            for i in range(m//2):
                H.swap_rows(i, m-1)

        return H

    def minimum_distance(self):
        r"""
        Return the minimum distance of ``self``.
        It is always 3 as ``self`` is a Hamming Code.

        EXAMPLES::

            sage: C = QAryHammingCode(GF(7), 3)
            sage: C.minimum_distance()
            3
        """
        return 3
