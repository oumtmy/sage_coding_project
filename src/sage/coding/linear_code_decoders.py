r"""
Linear Code Decoders
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from decoder import Decoder, DecodingFailure 

class DecoderLinearCodeSyndrome(Decoder):
    r"""
    Construct a decoder for Linear Codes. This decoder will use a syndrome
    based decoding algorithm.

    INPUT:

    - ``code`` -- A code associated to this decoder

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: D = DecoderLinearCodeSyndrome(C)
        sage: D
        Syndrome decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeSyndrome(C)
            sage: D
            Syndrome decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code, code.ambient_space(), "GeneratorMatrix",\
                {"hard-decision", "unique", "always-succeed", "complete"})

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeSyndrome(C)
            sage: D
            Syndrome decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Syndrome decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeSyndrome(C)
            sage: latex(D)
            \textnormal{Syndrome decoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Syndrome decoder for the }%s" % self.code()._latex_()

    def syndrome(self, r):
        r"""
        The vector r is a received word, so should be in the same ambient 
        space V as ``self.code()``. 
        Returns the all elements in V having the same syndrome (ie, the coset r+C, sorted by weight).

        INPUT:

        - ``r`` -- a vector of the ambient space of ``self.code()``

        OUTPUT:

        - a list of vectors
    
        EXAMPLES::

            sage: C = codes.HammingCode(2,GF(3))
            sage: V = VectorSpace(GF(3), 4)
            sage: r = V([0, 2, 0, 1])
            sage: D = DecoderLinearCodeSyndrome(C)
            sage: D.syndrome(r)
             [(0, 0, 1, 0), (0, 2, 0, 1), (2, 0, 0, 2), (1, 1, 0, 0), (2, 2, 2, 0), (1, 0, 2, 1), (0, 1, 2, 2), (1, 2, 1, 2), (2, 1, 1, 1)]
    
        """
        C = self.code()
        V = C.ambient_space()
        if not isinstance(r, list):
            r = r.list()
        r = V(r)
        coset = [[c + r, (c + r).hamming_weight()] for c in C]
        return [x[0] for x in sorted(coset, key=lambda x: x[1])]

    def decode_to_code(self, r):
        r"""
        Decode the received word ``r`` to an element in associated code of ``self``.

        INPUT:

        - ``r`` -- a vector of same length as the length of the associated
           code of ``self`` and over the base field of the associated code of ``self``

        OUTPUT:

        - a codeword of the associated code of ``self``

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5 
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeSyndrome(C)
            sage: r = vector(F, (8, 2, 6, 10, 6, 10, 7, 6, 7, 1))
            sage: D.decode_to_code(r) #long time
            (8, 2, 6, 10, 6, 10, 7, 6, 7, 1)
        """
        V = self.input_space()
        if not isinstance(r, list):
            r = r.list()
        r = V(r)
        c =  -V(self.syndrome(r)[0]) + r
        c.set_immutable()
        return c

    def decoding_radius(self):
        r"""
        Return maximal number of errors ``self`` can decode.

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5 
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeSyndrome(C)
            sage: D.decoding_radius()
            2
        """

        return (self.code().minimum_distance()-1) // 2










class DecoderLinearCodeNearestNeighbor(Decoder):
    r"""
    Construct a decoder for Linear Codes. This decoder will decode to the 
    nearest codeword found.

    INPUT:

    - ``code`` -- A code associated to this decoder
    
    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: D = DecoderLinearCodeNearestNeighbor(C)
        sage: D
        Nearest neighbor decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeNearestNeighbor(C)
            sage: D
            Nearest neighbor decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code, code.ambient_space(), "GeneratorMatrix",\
                {"hard-decision", "unique", "always-succeed", "complete"})

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeNearestNeighbor(C)
            sage: D
            Nearest neighbor decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Nearest neighbor decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeNearestNeighbor(C)
            sage: latex(D)
            \textnormal{Nearest neighbor decoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Nearest neighbor decoder for the }%s" % self.code()._latex_()

    def decode_to_code(self, r):
        r"""
        Decode the received word ``r`` to the nearest element in associated code of ``self``.

        INPUT:

        - ``r`` -- a vector of same length as the length of the associated
          code of ``self`` and over the base field of the associated code of ``self``

        OUTPUT:

        - a codeword of the associated code of ``self``

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5 
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeNearestNeighbor(C)
            sage: r = vector(F, (8, 2, 6, 10, 6, 10, 7, 6, 7, 1))
            sage: D.decode_to_code(r) #long time
            (8, 2, 6, 10, 6, 10, 7, 6, 7, 1)
        """
        V = self.input_space()
        if not isinstance(r, list):
            r = r.list()
        r = V(r)
        diffs = [[c - r, (c - r).hamming_weight()] for c in self.code()]
        diffs.sort(key=lambda x: x[1])
        c = diffs[0][0] + r
        c.set_immutable()
        return c

class DecoderLinearCodeTrivial(Decoder):
    r"""
    Construct a decoder for linear codes. This decoder will return the provided word if it 
    belongs to its associated code, else it will return an exception.

    INPUT:

    - ``code`` -- A code associated to this decoder
    
    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: D = DecoderLinearCodeTrivial(C)
        sage: D
        Trivial decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """
    
    def __init__(self, code):
        r"""

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeTrivial(C)
            sage: D
            Trivial decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code, code.ambient_space(),\
                "GeneratorMatrix", {"hard-decision", "unique"})

    def __eq__(self, other):
        r"""
        Test equality of DecoderLinearCodeTrivial objects.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D1 = DecoderLinearCodeTrivial(C)
            sage: D2 = DecoderLinearCodeTrivial(C)
            sage: D1 == D2
            True
        """
        return isinstance(other, DecoderLinearCodeTrivial)\
                and self.code() == other.code()\
                and self.input_space() == other.input_space()

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeTrivial(C)
            sage: D
            Trivial decoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Trivial decoder for the %s" % self.code() 

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: D = DecoderLinearCodeTrivial(C)
            sage: latex(D)
            \textnormal{Trivial decoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """  

        return "\\textnormal{Trivial decoder for the }%s" % self.code()._latex_()

    def decode_to_code(self, r):
        r"""
        Decode ``r`` to an element of ``self``.

        INPUT:

        - ``r`` -- a codeword of ``self``

        OUTPUT:

        - a vector of the message space of ``self``

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k) 
            sage: D = DecoderLinearCodeTrivial(C)
            sage: r = vector(F, (4, 7, 1, 1, 3, 6, 1, 4, 1, 3))
            sage: D.decode_to_code(r)
            (4, 7, 1, 1, 3, 6, 1, 4, 1, 3)
        
        Now if we take a word which is not in the code::

            sage: r[0] += 1
            sage: r in C
            False
            sage: D.decode_to_code(r)
            Traceback (most recent call last):
            ...
            DecodingFailure: Provided word is not in the code
        """
        if r in self.code():
            return r
        else:
            raise DecodingFailure("Provided word is not in the code")

    def decoding_radius(self):
        r"""
        Return maximal number of errors that ``self`` can decode.
        
        This decoder returns the word only if it is already on the code, so it
        is not able to decode any error.
        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k) 
            sage: D = DecoderLinearCodeTrivial(C)
            sage: D.decoding_radius()
            0
        """
        return 0
