r"""
Linear Code Encoders
"""

#*****************************************************************************
#       Copyright (C) 2015 David Lucas <david.lucas@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from encoder import Encoder
from sage.misc.cachefunc import cached_method

class EncoderLinearCodeGeneratorMatrix(Encoder):
    r"""
    Construct an encoder for Linear codes. 

    The only purpose of this encoder is to include the existing code
    into the new Encoder and Decoder structure.

    INPUT:

    - ``code`` -- The associated code of this encoder.

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: E = EncoderLinearCodeGeneratorMatrix(C) 
        sage: E
        Generator matrix-based encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """
    
    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderLinearCodeGeneratorMatrix(C) 
            sage: E
            Generator matrix-based encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code)

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderLinearCodeGeneratorMatrix(C) 
            sage: E
            Generator matrix-based encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Generator matrix-based encoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderLinearCodeGeneratorMatrix(C) 
            sage: latex(E)
            \textnormal{Generator matrix-based encoder for the }[40, 12, 29] \textnormal{ Generalized Reed-Solomon Code over } \Bold{F}_{59}
        """
        return "\\textnormal{Generator matrix-based encoder for the }%s" % self.code()._latex_()
    
    @cached_method
    def generator_matrix(self):
        r"""
        Returns a generator matrix of the associated code of ``self``.

        EXAMPLES::

            sage: F = GF(11)
            sage: n, k = 10, 5
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderLinearCodeGeneratorMatrix(C) 
            sage: E.generator_matrix()
            [1 1 1 1 1 1 1 1 1 1]
            [0 1 2 3 4 5 6 7 8 9]
            [0 1 4 9 5 3 3 5 9 4]
            [0 1 8 5 9 4 7 2 6 3]
            [0 1 5 4 3 9 9 3 4 5]
        """
        if hasattr(self.code(), "_generator_matrix"):
            return self.code()._generator_matrix
        else:
            return self.code().generator_matrix()









class EncoderLinearCodeParityCheck(Encoder):
    r"""
    Construct an encoder for Linear codes. 
    
    It constructs the generator matrix through the parity check matrix.

    INPUT:

    - ``code`` -- The associated code of this encoder.

    EXAMPLES::

        sage: F = GF(59)
        sage: n, k = 40, 12
        sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
        sage: E = EncoderLinearCodeParityCheck(C) 
        sage: E
        Parity check matrix-based encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
    """

    def __init__(self, code):
        r"""
        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderLinearCodeParityCheck(C) 
            sage: E
            Parity check matrix-based encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        self._initialize_parent(code)

    def __repr__(self):
        r"""
        Return a string representation of ``self``.

        EXAMPLES::

            sage: F = GF(59)
            sage: n, k = 40, 12
            sage: C = GeneralizedReedSolomonCode(F.list()[:n], k)
            sage: E = EncoderLinearCodeParityCheck(C) 
            sage: E
            Parity check matrix-based encoder for the [40, 12, 29] Generalized Reed-Solomon Code over Finite Field of size 59
        """
        return "Parity check matrix-based encoder for the %s" % self.code()

    def _latex_(self):
        r"""
        Return a latex representation of ``self``.

        EXAMPLES::

            sage: C = QAryHammingCode(GF(3), 3) 
            sage: E = EncoderLinearCodeParityCheck(C) 
            sage: E = EncoderLinearCodeParityCheck(C) 
            sage: latex(E)
            \textnormal{Parity check matrix-based encoder for the }[13 10] \textnormal{ Q-ary Hamming Code}
        """
        return "\\textnormal{Parity check matrix-based encoder for the }%s" % self.code()._latex_()

    @cached_method
    def generator_matrix(self):
        r"""
        Returns a generator matrix of the associated code of ``self``.

        EXAMPLES::

            sage: C = QAryHammingCode(GF(3), 3) 
            sage: E = EncoderLinearCodeParityCheck(C) 
            sage: E.generator_matrix()
            [1 0 0 0 0 0 0 0 0 0 1 2 0]
            [0 1 0 0 0 0 0 0 0 0 0 1 2]
            [0 0 1 0 0 0 0 0 0 0 1 0 2]
            [0 0 0 1 0 0 0 0 0 0 1 1 1]
            [0 0 0 0 1 0 0 0 0 0 1 1 2]
            [0 0 0 0 0 1 0 0 0 0 2 0 2]
            [0 0 0 0 0 0 1 0 0 0 1 2 1]
            [0 0 0 0 0 0 0 1 0 0 2 1 1]
            [0 0 0 0 0 0 0 0 1 0 2 2 0]
            [0 0 0 0 0 0 0 0 0 1 0 1 1]
        """
        return self.code().parity_check_matrix().right_kernel_matrix()
