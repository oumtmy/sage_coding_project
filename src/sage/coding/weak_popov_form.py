#Temporary file for weak_popov_form computation and use
#Will be removed when tickets #16742 and 16888 will be reviewed
#Directly imported from Johan S. R. Nielsen's coding lib
#See https://bitbucket.org/jsrn/codinglib
from sage.rings.integer import Integer
from sage.functions.other import floor

def module_mulders_storjohann(M, weights=None, debug=0):

    if weights and len(weights) != M.ncols():
        raise ValueError("The number of weights must equal the number of columns")
    # initialise conflicts list and LP map
    LP_to_row = dict( (i,[]) for i in range(M.ncols()))
    conflicts = []
    for i in range(M.nrows()):
        lp = LP(M.row(i), weights=weights)
        ls = LP_to_row[lp]
        ls.append(i)
        if len(ls) > 1:
            conflicts.append(lp)
        iters = 0
    # while there is a conflict, do a row reduction
    while conflicts:
        lp = conflicts.pop()  
        ls = LP_to_row[lp]
        i, j = ls.pop(), ls.pop()    
        if M[i,lp].degree() < M[j, lp].degree():
            j,i = i,j

        module_row_reduction(M, i, j, lp)
        ls.append(j)
        lp_new = LP(M.row(i), weights=weights)
        if lp_new > -1:
            ls_new = LP_to_row[lp_new]
            ls_new.append(i)
            if len(ls_new) > 1:
                conflicts.append(lp_new)
            iters += 1
    return iters
    
def module_weak_popov(M, weights=None, debug=0):
    return module_mulders_storjohann(M, weights=weights, debug=debug)

def module_apply_weights(M, weights):
    x = M.base_ring().gen()
    if all( w.is_integer() for w in weights):
        for j in range(M.ncols()):
            M.set_col_to_multiple_of_col(j,j, x**weights[j])
    else:
        perm = module_fractional_weight_permutation(weights)
        for j in range(M.ncols()):
            M.set_col_to_multiple_of_col(j,j, x**floor(weights[j]))  
            M.permute_columns(perm)
            return perm

def module_remove_weights(M, weights):
    if all( w.is_integer() for w in weights):
        for i in range(M.nrows()):
            for j in range(M.ncols()):
                M[i,j] = M[i,j].shift(-weights[j])
    else:
        perm = module_fractional_weight_permutation(weights)
        pinv = perm.inverse()
        M.permute_columns(pinv)
        module_remove_weights(M, [ floor(wj) for wj in weights ])

def module_row_reduction(M, i, j, pos):
    pow = M[i,pos].degree() - M[j,pos].degree()
    if pow < 0:
        return None
    coeff = -M[i, pos].leading_coefficient() / M[j, pos].leading_coefficient()
    x = M.base_ring().gen()
    multiple = x**pow*coeff
    M.add_multiple_of_row(i, j, multiple)
    return x**pow*coeff

def module_fractional_weight_permutation(weights):
    n = len(weights)
    denominator = lcm(list(f.denominator() for f in weights))
    numerators = [ f.numerator() * denominator/f.denominator() for f in weights ]
    residues = [ num % denominator for num in numerators ]
    res_map = dict()
    for i in range(n):
        if residues[i] in res_map:
            res_map[residues[i]].append(i+1)
        else:
            res_map[residues[i]] = [i+1]
    res_uniq = sorted(res_map.keys())
    return Permutation(list(flatten_once([ res_map[res] for res in res_uniq ])))

def LP(v, weights=None):
    if not weights:
        weights=[0]*len(v)
    best=-1
    bestp=-1
    for p in range(0,len(v)):
        if not v[p].is_zero():
            vpdeg = v[p].degree() + weights[p]
            if vpdeg >= best:
                best=vpdeg
                bestp = p
    if best==-1:
        return -1
    else:
        return bestp
